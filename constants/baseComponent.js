import {StyleSheet} from 'react-native';

export const Base = {
  primary: '#e93766',
  titleFontSize: 40,
  subTitleFontSize: 20,
  captionFontSize: 12,
};

export const baseStyle = StyleSheet.create({
  baseContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: Base.primary,
    fontSize: Base.titleFontSize,
  },
  subTitle: {
    color: Base.primary,
    fontSize: Base.subTitleFontSize,
  },
  baseButton: {
    color: Base.primary,
  },
  baseButtonContainer: {
    borderColor: Base.primary,
    borderStyle: 'solid',
    borderWidth: 1,
  },
  caption: {
    fontSize: Base.captionFontSize,
  },
  buttonContainerOverlay: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end',
    paddingVertical: 20,
    alignItems: 'flex-end',
  },
  textOverlay: {
    textAlign: 'justify',
  },
  titleOverlayContainer: {
    paddingBottom: 10,
  },
});
