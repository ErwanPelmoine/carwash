const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.database();
const fcm = admin.messaging();
// TWILIO
const twilio = require('twilio');
const accountSid = 'AC369ccc8182188ee1c35d63b252c22294'; // Your Account SID from www.twilio.com/console
const authToken = '2a1dc9f98535035fdd14e778d88eb372'; // Your Auth Token from www.twilio.com/console
const client = new twilio(accountSid, authToken);

exports.helloWorld = functions.firestore
  .document('users/{userId}/{orders}/{orderId}')
  .onCreate((snap, context) => {
    const newValue = snap.data();

    client.messages
      .create({
        body: `Bonjour, un lavage a été demandé !\n
        Adresse : ${newValue.address}\n
        Date : ${newValue.orderDateTimeDisplayed}\n
        Si vous êtes disponible répondez : Go !`,
        to: 'whatsapp:+33658219795', // Text this number
        from: 'whatsapp:+14155238886', // From a valid Twilio number
      })
      .then(message =>
        console.log('new message id : ', message.sid + ' message : ', message),
      )
      .catch(error => console.log(error));
  });
//AC369ccc8182188ee1c35d63b252c22294
//2a1dc9f98535035fdd14e778d88eb372

exports.useWildcard = functions.firestore
  .document('users/{userId}')
  .onWrite((change, context) => {
    // If we set `/users/marie` to {name: "Marie"} then
    // context.params.userId == "marie"
    // ... and ...
    // change.after.data() == {name: "Marie"}
  });
