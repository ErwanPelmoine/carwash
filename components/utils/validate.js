export function valideString(stringToValidate) {
  return (
    stringToValidate !== undefined &&
    stringToValidate !== null &&
    stringToValidate !== ''
  );
}
