import React, {useState} from 'react';
import {Text, TextInput, View, Button} from 'react-native';
import styles from '../login-component/style.js';
import {baseStyle} from '../../../constants/baseComponent';
import auth from '@react-native-firebase/auth';
import {valideString} from '../../utils/validate';
import firestore from '@react-native-firebase/firestore';

export default function SignUp(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState(null);
  const refUsers = firestore().collection('users');

  function handleSignUp() {
    if (valideString(email) && valideString(password)) {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(async () => {
          await refUsers.doc(email).set({
            email: email,
            isWasher: false,
          });
          props.navigation.navigate('Lavage');
        })
        .catch(error => {
          if (error.code === 'auth/email-already-in-use') {
            setErrorMessage(
              'Cette email est déjà utilisé par un autre compte.',
            );
          } else {
            setErrorMessage(error.message);
          }
        });
    } else {
      setErrorMessage(
        "Merci d'insérer votre adresse email et votre mot de passe.",
      );
    }
  }

  return (
    <View style={styles.container}>
      <View style={[styles.titleContainer]}>
        <Text style={baseStyle.title}>S'enregistrer</Text>
        {errorMessage && <Text style={{color: 'red'}}>{errorMessage}</Text>}
      </View>
      <View style={[styles.inputContainer]}>
        <TextInput
          placeholder="Email"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={newEmail => setEmail(newEmail)}
          value={email}
        />
        <TextInput
          secureTextEntry
          placeholder="Password"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={newPassword => setPassword(newPassword)}
          value={password}
        />
        <View style={[styles.buttonContainer]}>
          <Button title="Création" color="#e93766" onPress={handleSignUp} />
        </View>
      </View>
      <View style={[styles.textContainerTextBottom]}>
        <Text style={styles.sizeCaption}>
          {' '}
          Vous avez déjà un compte ?&nbsp;
          <Text
            onPress={() => props.navigation.navigate('Login')}
            style={[styles.textPrimary, styles.sizeCaption]}>
            Je me connecte
          </Text>
        </Text>
      </View>
    </View>
  );
}

SignUp.navigationOptions = {
  headerVisible: false,
};
