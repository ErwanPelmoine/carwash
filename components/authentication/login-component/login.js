import React, {useState} from 'react';
import {Text, TextInput, View} from 'react-native';
import styles from './style';
import {Overlay, Button} from 'react-native-elements';
import {baseStyle} from '../../../constants/baseComponent';
import auth from '@react-native-firebase/auth';
import {firebase} from '@react-native-firebase/auth';
import {valideString} from '../../utils/validate';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import firestore from '@react-native-firebase/firestore';

export default function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState(null);
  const [displayOverlay, setDisplayOverlay] = useState(false);
  const [credential, setCredential] = useState(null);
  const [hasTryConnectedFacebook, setHasTryConnectedFacebook] = useState(false);
  const refUsers = firestore().collection('users');

  function handleLogin() {
    console.log('handle login');
    console.log(valideString(email));
    console.log(valideString(password));

    if (valideString(email) && valideString(password)) {
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => console.log('success signInWithEmailAndPassword'))
        .catch(error =>
          console.log('error during signInWithEmailAndPassword', error),
        );
    } else {
      setErrorMessage(
        "Merci d'insérer votre adresse email et votre mot de passe.",
      );
    }
  }

  async function allowMultipleProvider(email, credential) {
    console.log('cred : ', credential);
    const providers = await firebase.auth().fetchSignInMethodsForEmail(email);
    if (providers.includes('password')) {
      setEmail(email);
      setCredential(credential);
      setDisplayOverlay(true);
      setHasTryConnectedFacebook(true);
    }
  }

  function signInWithFacebook(fbToken) {
    return new Promise((resolve, reject) => {
      const currentCredential = firebase.auth.FacebookAuthProvider.credential(fbToken);
      firebase
        .auth()
        .signInWithCredential(currentCredential)
        .catch(function(error) {
          console.log("error lors de l'authentification firebase : ", error);
          console.log(Object.values(error));
          console.log('codeError : ', error.credential);
          console.log('emailError : ', error.email);
          const codeError = error.code;
          const emailError = error.email;
          if (codeError === 'auth/account-exists-with-different-credential') {
            allowMultipleProvider(emailError, currentCredential)
              .then(function(result) {
                console.log('success allowMultipleProvider : ', result);
              })
              .catch(function(result) {
                console.log('error allowMultipleProvider : ', result);
              });
          }
          reject(error.code);
        });
    });
  }

  async function handleLoginFacebook() {
    console.log('handleLoginFacebook');
    LoginManager.logInWithPermissions(['public_profile', 'email'])
      .then(() =>
        AccessToken.getCurrentAccessToken()
          .then(data => {
            if (!data) {
              // handle this however suites the flow of your app
              throw new Error(
                'Something went wrong obtaining the users access token',
              );
            }
            signInWithFacebook(data.accessToken)
              .then(function(result) {
                console.log(
                  'success signInWithFacebook : ',
                  result.exists + ' user : ',
                  result.user,
                );
                if (result.exists) {
                  console.log('exist connected : ', result.user);
                } else {
                  console.log('not exist connected : ', result.user);
                }
              })
              .catch(function(error) {
                console.debug('error  signInWithFacebook : ', error);
              });
          })
          .catch(error =>
            console.debug('error  getCurrentAccessToken : ', error),
          ),
      )
      .catch(error => console.debug('error  logInWithPermissions : ', error));
  }

  function onPressValidateButton() {
    setDisplayOverlay(false);
  }
  return (
    <View style={styles.container}>
      <View style={[styles.titleContainer]}>
        <Text style={baseStyle.title}>Se connecter</Text>
        {errorMessage && <Text style={{color: 'red'}}>{errorMessage}</Text>}
      </View>
      <View style={[styles.inputContainer]}>
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={newEmail => setEmail(newEmail)}
          value={email}
        />
        <TextInput
          secureTextEntry
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Password"
          onChangeText={newPassword => setPassword(newPassword)}
          value={password}
        />
        <View style={[styles.textContainer]}>
          <Text
            onPress={
              () =>
                LoginManager.logOut() /*props.navigation.navigate('ForgotPassword')*/
            }
            style={[styles.textPrimary, styles.sizeCaption]}>
            Je retrouve mon mot de passe
          </Text>
        </View>
        <View style={[styles.buttonContainer]}>
          <Button
            title="CONNEXION"
            type="clear"
            titleStyle={baseStyle.baseButton}
            containerStyle={baseStyle.baseButtonContainer}
            onPress={handleLogin}
          />
        </View>

        <View style={[styles.dividerContainer]}>
          <Text style={[styles.dividerText, styles.sizeCaption]}>
            Autre méthode de connexion
          </Text>
        </View>
        <View style={[styles.buttonContainer]}>
          <Button
            title="FACEBOOK"
            type="clear"
            titleStyle={baseStyle.baseButton}
            containerStyle={baseStyle.baseButtonContainer}
            onPress={handleLoginFacebook}
          />
        </View>
      </View>
      <View style={[styles.textContainerTextBottom]}>
        <Text style={[styles.sizeCaption]}>
          {' '}
          Vous n'avez pas encore de compte ?&nbsp;
          <Text
            onPress={() => props.navigation.navigate('SignUp')}
            style={[styles.textPrimary, styles.sizeCaption]}>
            Je m'enregistre
          </Text>
        </Text>
      </View>
      <Overlay
        isVisible={displayOverlay}
        width={300}
        height={200}
        onBackdropPress={() => setDisplayOverlay(false)}>
        <View>
          <View style={styles.titleOverlayContainer}>
            <Text style={baseStyle.subTitle}>Compte déjà existant </Text>
          </View>
          <Text style={styles.textOverlay}>
            Un compte existe déjà avec votre adresse {email}.{'\n'}
            {'\n'}Merci de rentrer votre mot de passe afin de connecter vos
            comptes ensembles.
          </Text>
          <View style={styles.buttonContainerOverlay}>
            <Button
              title="VALIDER"
              onPress={onPressValidateButton}
              type="clear"
              titleStyle={baseStyle.baseButton}
            />
          </View>
        </View>
      </Overlay>
    </View>
  );
}
Login.navigationOptions = {
  header: null,
};
