import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 25,
  },
  titleContainer: {
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
  inputContainer: {
    display: 'flex',
    flexGrow: 1,
    width: '100%',
  },
  textInput: {
    height: 40,
    fontSize: 15,
    width: '100%',
    borderColor: '#9b9b9b',
    borderBottomWidth: 1,
    marginVertical: 10,
  },
  textPrimary: {
    color: '#e93766',
  },
  textContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-start',
  },
  sizeCaption: {
    fontSize: 12,
  },
  buttonContainer: {
    marginVertical: 20,
  },
  contour: {
    borderColor: 'red',
    borderStyle: 'solid',
    borderWidth: 1,
  },
  textContainerTextBottom: {
    display: 'flex',
    flexGrow: 1,
    width: '100%',
    justifyContent: 'flex-end',
    paddingBottom: 30,
    alignItems: 'flex-start',
  },
  dividerContainer: {
    width: '100%',
    height: 11,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    alignItems: 'center',
  },
  dividerText: {
    paddingHorizontal: 10,
    backgroundColor: '#FFFFFF',
  },
});
