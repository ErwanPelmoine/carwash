import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View, Button} from 'react-native';
import styles from './../login-component/style';
//import firebase from 'react-native-firebase';

export default function ForgotPassword(props) {
  const [email, setEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState(undefined);
  function initPassword() {
  /*  firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => props.navigation.navigate('Login'))
      .catch(error => setErrorMessage(error.message));*/
  }

  return (
    <View style={styles.container}>
      <View style={[styles.titleContainer]}>
        <Text style={{color: '#e93766', fontSize: 40}}>
          Mot de passe oublié
        </Text>
        {errorMessage && <Text style={{color: 'red'}}>{errorMessage}</Text>}
      </View>
      <View style={[styles.inputContainer]}>
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          placeholder="Email"
          onChangeText={newEmail => setEmail(newEmail)}
          value={email}
        />
        <View style={[styles.buttonContainer]}>
          <Button
            title="Réinitialisation"
            color="#e93766"
            onPress={initPassword}
          />
        </View>
      </View>
      <View style={[styles.textContainerTextBottom]} />
    </View>
  );
}
ForgotPassword.navigationOptions = {
  headerStyle: {
    backgroundColor: '#FFFFFF',
    borderBottom: 'none',
  },
};
