import React, {Component} from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import auth from '@react-native-firebase/auth';
import {PERMISSIONS, request} from 'react-native-permissions';
export default class Loading extends Component {
  componentDidMount() {
    auth().onAuthStateChanged(user => {
      request(PERMISSIONS.ANDROID.RECORD_AUDIO).then(r => console.log(r));
      console.log('onAuthStateChanged');
      /*      this.props.navigation.navigate(user ? 'Lavage' : 'Login');*/
      this.props.navigation.navigate(user ? 'Lavage' : 'Login');
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: '#e93766', fontSize: 40}}>Un instant</Text>
        <ActivityIndicator color="#e93766" size="large" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
