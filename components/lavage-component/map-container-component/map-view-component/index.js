import React, {useEffect, useState} from 'react';
import MapView, {Marker} from 'react-native-maps';

export default function MyMapView(props) {
  const [latitude, setLatitude] = useState(37.78825);
  const [longitude, setLongitude] = useState(-122.4324);
  const [marker, setMarker] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
  });

  useEffect(() => {
    console.log('use lat :', props.latitude);
    const newLatitude =
      props.latitude === undefined ? 37.78825 : props.latitude;
    const newLongitude =
      props.longitude === undefined ? -122.4324 : props.longitude;
    setLatitude(newLatitude);
    setLongitude(newLongitude);
    setMarker({longitude: newLongitude, latitude: newLatitude});
    console.log(
      'newLatitude : ',
      newLatitude + ' newLongitude : ',
      newLongitude,
    );
  }, [props.latitude, props.longitude]);

  /* function onRegionChange(data) {
    setRegion({...region, longitude: data.longitude, latitude: data.latitude});
  }*/
  function onDragMarker(e) {
    setMarker(e.nativeEvent.coordinate);
    setLatitude(e.nativeEvent.coordinate.latitude);
    setLongitude(e.nativeEvent.coordinate.longitude);
    props.onChangePlace(
      e.nativeEvent.coordinate.latitude,
      e.nativeEvent.coordinate.longitude,
    );
  }
  return (
    <>
      <MapView
        style={{flex: 1, width: '100%'}}
        region={{
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        <Marker draggable coordinate={marker} onDragEnd={onDragMarker} />
      </MapView>
    </>
  );
}
