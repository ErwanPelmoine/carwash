import React, {useEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, Text, View} from 'react-native';
import PropTypes from 'prop-types';
import {withNavigation} from 'react-navigation';

function ButtonAdress(props) {
  const [placeName, setPlaceName] = useState('');
  const [placeSecondName, setPlaceSecondName] = useState('');
  const [quartier, setQuartier] = useState('');
  const [city, setCity] = useState('');
  const [address, setAddress] = useState(props.newAddress);

  useEffect(() => {
    console.log('test');

    if (address !== undefined) {
      const newPlaceName =
        address.address_components[0].short_name +
        ' ' +
        address.address_components[1].short_name;
      const newPlaceSecondName = address.address_components[2].short_name;
      const newQuartier = address.address_components[3].short_name;
      setPlaceName(newPlaceName);
      setPlaceSecondName(newPlaceSecondName);
      setQuartier(newQuartier);
      console.log(newPlaceName + '-' + newPlaceSecondName + '-' + newQuartier);
      //setCity(address.address_components[4].short_name);
    }
  }, [address]);

  const onPressAdressButton = () => {
    props.navigation.navigate('MapInput', {notifyChange: notifyChange});
  };

  const notifyChange = (location, formatedAdress) => {
    setAddress(formatedAdress);
    props.setNewLocation(location);
  };

  return (
    <TouchableOpacity style={styles.container} onPress={onPressAdressButton}>
      <View>
        <Text>{placeName}</Text>
      </View>
      <View>
        <Text>
          {placeSecondName} - {quartier} - {city}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: 'white',
  },
});

ButtonAdress.propTypes = {
  newAddress: PropTypes.object,
  navigation: PropTypes.object.isRequired,
};

export default withNavigation(ButtonAdress);
