import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import MyMapView from './map-view-component/index';
import MapInput from './map-input-component/index';
import Geolocation from '@react-native-community/geolocation';
import ButtonAdress from './button-adress-component/index';
import Geocoder from 'react-native-geocoding';
Geocoder.init('AIzaSyCuYRl4XxF6nP4CgZG43lig2GdnAzgtHck');

export default function MapContainer(props) {
  const [latitude, setLatitude] = useState(undefined);
  const [longitude, setLongitude] = useState(undefined);
  //const [locationFromInput, setLocationFromInput] = useState(undefined);
  const [address, setAddress] = useState(undefined);

  useEffect(() => {
    getLocation().then(location => {
      console.log('getLocation : ', location);
      Geocoder.from(location)
        .then(json => {
          setLatitude(location.latitude);
          setLongitude(location.longitude);
          setAddress(json.results[0]);
        })
        .catch(error => console.warn(error));
    });
  }, []);

  function getLocation() {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition(
        data => {
          resolve(data.coords);
        },
        err => {
          reject(err);
        },
      );
    });
  }

  const setNewLocation = newLocation => {
    console.log('received location from google api : ', newLocation);
    setLatitude(newLocation.lat);
    setLongitude(newLocation.lng);
  };
  /*function saveCoordAndAdress(newLocation, adress) {
    console.log('newLocation GPS  : ', newLocation);
    console.log('newLocation adress  : ', adress);

    setLocationFromInput(newLocation);
    setLatitude(newLocation.lat);
    setLongitude(newLocation.lng);
    onChangePlace(newLocation.lat, newLocation.lng, adress);
  }*/
  function onChangePlace(newLatitude, newLongitude, adress) {
    props.onChangePlace(newLatitude, newLongitude, adress);
  }
  return (
    <View style={styles.mainContainer}>
      {/*<View style={[styles.titleContainer]}>
        <Text style={styles.titleFont}>Lieux de stationnement</Text>
  </View>*/}
      <View style={styles.mapAndInputContainer}>
        <View style={styles.mapContainer}>
          {latitude && (
            <MyMapView
              latitude={latitude}
              longitude={longitude}
              onChangePlace={(x, y) => onChangePlace(x, y)}
            />
          )}
        </View>
        <View style={styles.inputContainer}>
          {address && (
            <ButtonAdress
              newAddress={address}
              setNewLocation={setNewLocation}
            />
          )}
          {/*<MapInput
            notifyChange={(newLocation, adress) => {
              if (newLocation !== locationFromInput) {
                saveCoordAndAdress(newLocation, adress);
              }
            }}
          />*/}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    alignItems: 'flex-start',
    marginBottom: 15,
  },
  titleFont: {
    fontWeight: 'bold',
  },
  mainContainer: {
    display: 'flex',
    width: '100%',
    height: '100%',
    zIndex: 100,
  },
  mapAndInputContainer: {
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  mapContainer: {
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  inputContainer: {
    width: '100%',
    minHeight: 300,
    display: 'flex',
    position: 'absolute',
    zIndex: 1000,
  },
});
