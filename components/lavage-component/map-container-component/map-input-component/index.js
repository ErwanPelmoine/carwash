import React from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {StyleSheet} from 'react-native';
import MDCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import PropTypes from 'prop-types';
import {withNavigation} from 'react-navigation';
navigator.geolocation = require('@react-native-community/geolocation');

function MapInput(props) {
  return (
    <GooglePlacesAutocomplete
      placeholder={'Rechercher'}
      currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel={'Position actuelle'}
      minLength={2}
      autoFocus={false}
      returnKeyType={'search'}
      listViewDisplayed={true}
      fetchDetails={true}
      onPress={(data, details = null) => {
        const notifyChange = props.navigation.getParam('notifyChange');
        notifyChange(details.geometry.location, details);
        props.navigation.navigate('Lavage');

        /* notifyChange(
          details.geometry.location,
          details.formatted_address,
        );*/
      }}
      query={{
        key: 'AIzaSyCuYRl4XxF6nP4CgZG43lig2GdnAzgtHck',
        lang: 'ma',
      }}
      nearbyPlacesAPI={'GooglePlacesSearch'}
      debounce={0}
    />
  );
}

const styles = StyleSheet.create({
  textInputContainer: {
    backgroundColor: 'rgba(0,0,0,0)',
    borderTopWidth: 0,
    borderBottomWidth: 0,
    width: '90%',
  },
  textInput: {
    marginLeft: 0,
    marginRight: 0,
    height: 40,
    backgroundColor: '#fafafa',

    fontSize: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#9b9b9b',
  },
  predefinedPlacesDescription: {
    color: '#1faadb',
  },
  listView: {
    width: '90%',
    backgroundColor: 'white',
  },

  textIcon: {
    color: 'grey',
    display: 'flex',
    alignSelf: 'flex-end',
  },
});

MapInput.propTypes = {
  notifyChange: PropTypes.func.isRequired,
};

export default withNavigation(MapInput);
