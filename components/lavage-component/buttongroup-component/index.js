import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import Button from './button-component/index';
import IonicIcon from 'react-native-vector-icons/Ionicons';

const buttons = [
  {
    index: 0,
    icon: 'ionic',
    iconName: 'logo-model-s',
    secondIcon: 'mdc',
    secondIconName: 'steering',
    text: 'COMPLET',
    value: 'complet',
  },
  {
    index: 1,
    icon: 'mdc',
    iconName: 'steering',
    text: 'INTÉRIEUR',
    value: 'interieur',
  },
  {
    index: 2,
    icon: 'ionic',
    iconName: 'logo-model-s',
    text: 'EXTÉRIEUR',
    value: 'exterieur',
  },
];
export default function ButtonGroupLavage(props) {
  const [selectedIndex, setSelectedIndex] = useState(0);
  function updateIndexSelected(newSelectedIndex) {
    setSelectedIndex(newSelectedIndex);
    props.onChangeTypeOfCarWash(buttons[newSelectedIndex].value);
  }
  return (
    <View>
      <View style={[styles.titleContainer]}>
        <Text style={styles.titleFont}>Type de lavage</Text>
        <TouchableOpacity onPress={props.showOverlayLavageInformation}>
          <IonicIcon
            name="md-information-circle-outline"
            size={20}
            color="grey"
          />
        </TouchableOpacity>
      </View>
      <View style={styles.containerButton}>
        {buttons.map((button, index) => (
          <Button
            button={button}
            key={index}
            updateIndexSelected={updateIndexSelected}
            selectedIndex={selectedIndex}
          />
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerButton: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'red',
    borderRadius: 4,
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    alignItems: 'flex-start',
    marginBottom: 15,
  },
  titleFont: {
    fontWeight: 'bold',
    paddingRight: 10,
  },
  textIcon: {
    color: 'grey',
  },
});
