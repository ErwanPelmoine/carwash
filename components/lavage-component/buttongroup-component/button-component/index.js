import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Base} from '../../../../constants/baseComponent';
export default function Button(props) {
  const [isSelected, setIsSelected] = useState(false);
  const {button} = props;

  useEffect(() => {
    if (props.selectedIndex === props.button.index) {
      setIsSelected(true);
    } else {
      setIsSelected(false);
    }
  }, [props.button.index, props.selectedIndex]);

  function onPressButton() {
    props.updateIndexSelected(props.button.index);
  }
  function getBackgroundColorButton() {
    if (isSelected) {
      return {backgroundColor: Base.primary};
    } else {
      return {backgroundColor: 'white'};
    }
  }

  function getColorText() {
    if (isSelected) {
      return {color: 'white'};
    } else {
      return {color: 'black'};
    }
  }

  return (
    <TouchableOpacity
      style={[getBackgroundColorButton(), styles.touchableButtonContainer]}
      onPress={onPressButton}>
      <View style={styles.buttonTextContainer}>
        <Text style={[getColorText(), styles.buttonText]}>{button.text}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  touchableButtonContainer: {
    width: '33.3%',
    height: 45,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: Base.primary,
  },
  iconContainer: {
    display: 'flex',
    justifyContent: 'center',
    marginHorizontal: 5,
  },
  buttonTextContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
});
