import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import ButtonGroupLavage from './buttongroup-component/index';
import {Base} from '../../constants/baseComponent';
import TextInputDateTimePicker from './date-time-picker-component/index';
import OverlayLavageInformation from './overlay-lavage-information/index';
import MapContainer from './map-container-component/index';
import {Icon} from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import {firebase} from '@react-native-firebase/auth';

export default function Lavage(props) {
  const [
    isOverlayLavageInformationVisible,
    setIsOverlayLavageInformationVisible,
  ] = useState(false);
  const [orderType, setOrderType] = useState('complet');
  const [orderDateTime, setOrderDateTime] = useState(new Date());
  const [orderDateTimeDisplayed, setOrderDateTimeDisplayed] = useState();
  const [place, setPlace] = useState({});
  const [address, setAddress] = useState({});

  const refUsers = firestore().collection('users');

  function showOverlayLavageInformation() {
    setIsOverlayLavageInformationVisible(true);
  }

  function onChangeTypeOfCarWash(newOrderType) {
    setOrderType(newOrderType);
  }
  function onChangeDateTime(newDateTime) {
    setOrderDateTime(newDateTime);
    setOrderDateTimeDisplayed(getOrderDateTimeDisplayed(newDateTime));
  }
  function getOrderDateTimeDisplayed(newDateTime) {
    return new Intl.DateTimeFormat('fr-FR', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    }).format(newDateTime);
  }
  function onChangePlace(newLatitude, newLongitude, newAddress) {
    setPlace({newLatitude, newLongitude});
    setAddress(newAddress);
  }

  async function onPressButtonOrderWashing() {
    const order = {
      orderType: orderType,
      orderDateTime: orderDateTime,
      place: place,
      currentDateTime: new Date(),
      user: firebase.auth().currentUser.email,
      address: address,
      orderDateTimeDisplayed: orderDateTimeDisplayed,
    };
    console.log(
      'save an order to wash a car at ' +
        order.address +
        '\n the ' +
        order.orderDateTimeDisplayed +
        '\n for user : ' +
        firebase.auth().currentUser.email,
    );
    await refUsers
      .doc(firebase.auth().currentUser.email)
      .collection('orders')
      .doc(new Date().toISOString())
      .set(order);
  }
  return (
    <View style={styles.containerHome}>
      {/*<ButtonGroupLavage
        showOverlayLavageInformation={showOverlayLavageInformation}
        onChangeTypeOfCarWash={onChangeTypeOfCarWash}
      />
      <TextInputDateTimePicker onChangeDateTime={onChangeDateTime} />*/}
      <MapContainer onChangePlace={onChangePlace} />
      {/* <TouchableOpacity
        style={{position: 'absolute', right: 0, bottom: 0, zIndex: 200}}
        onPress={onPressButtonOrderWashing}>
        <Icon
          reverse
          button
          name="sc-telegram"
          type="evilicon"
          color="#e93766"
        />
     </TouchableOpacity>*/}
      <OverlayLavageInformation
        isVisible={isOverlayLavageInformationVisible}
        setIsVisible={setIsOverlayLavageInformationVisible}
      />
    </View>
  );
}

Lavage.navigationOptions = {
  title: 'Lavage',
};

const styles = StyleSheet.create({
  containerHome: {
    display: 'flex',
    width: '100%',
    height: '100%',
    backgroundColor: '#fafafa',
    borderWidth: 1,
    borderColor: 'red',
  },

  textInputContaine: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    overflow: 'hidden',
  },
  textInput: {
    height: 40,
    fontSize: 15,
    borderColor: '#9b9b9b',
    borderBottomWidth: 1,
    marginVertical: 15,
    marginLeft: 5,
    width: '100%',
  },
  buttonValidate: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  buttonValidateIcon: {
    color: Base.primary,
  },
});
