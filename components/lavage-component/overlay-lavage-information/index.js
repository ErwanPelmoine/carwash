import {StyleSheet, Text, View} from 'react-native';
import {Button, Overlay} from 'react-native-elements';
import React, {useEffect, useState} from 'react';
import {baseStyle} from '../../../constants/baseComponent';

export default function OverlayLavageInformation(props) {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    setIsVisible(props.isVisible);
  }, [props.isVisible]);

  function closeOverlay() {
    props.setIsVisible(false);
  }
  return (
    <Overlay
      isVisible={isVisible}
      width={300}
      height="auto"
      onBackdropPress={closeOverlay}>
      <View>
        <View style={styles.titleOverlayContainer}>
          <Text style={baseStyle.subTitle}>Détails de nos prestations</Text>
        </View>
        <Text style={styles.subTitleTextOverlay}>Lavage intérieur</Text>
        <Text style={styles.textOverlay}>
          Notre "Washer" passera l'aspirateur sur les moquettes, les sièges et
          les tapis. Puis, il nettoiera les portières intérieur plastique et
          carrosserie. Il finira par les vitres et le tableau de bord.{'\n'}
          Tous nos entretiens sont effectués à l'aide de tissus micro-fibre
          ainsi qu’avec des produits anti-uv afin d'éviter le vieillissement et
          le craquement des moulures en plastique et en caoutchouc. Votre
          voiture retrouvera son aspect d’origine.
        </Text>
        <Text style={styles.subTitleTextOverlay}>Lavage extérieur</Text>
        <Text style={styles.textOverlay}>
          Notre "Washer" commencera par laver vos pneus et jantes à l'aide de
          brosses et d'un savon particulier. Puis, il appliquera un savon de
          pré-lavage sur votre carosserie. Enfin, après rinçage il enlèvera avec
          un gant en micro-fibre et un savon de lavage la majorité des
          impuretés. Il sechera ensuite votre véhicule avec une micro-fibre.
        </Text>
        <Text style={styles.subTitleTextOverlay}>Lavage complet</Text>
        <Text style={styles.textOverlay}>
          Le lavage complet comprends toutes les prestations du lavage intérieur
          et extérieur.
        </Text>
        <View style={styles.buttonContainerOverlay}>
          <Button
            title="FERMER"
            onPress={closeOverlay}
            type="clear"
            titleStyle={baseStyle.baseButton}
          />
        </View>
      </View>
    </Overlay>
  );
}
const styles = StyleSheet.create({
  subTitleTextOverlay: {
    fontWeight: 'bold',
  },
  textOverlay: {
    marginBottom: 10,
    textAlign: 'justify',
  },
});
