import React, {useState} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import MDCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import DateTimePicker from 'react-native-modal-datetime-picker';
import 'intl';
import 'intl/locale-data/jsonp/fr-FR';

export default function TextInputDateTimePicker(props) {
  const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
  const [isTimePickerVisible, setIsTimePickerVisible] = useState(false);
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [dateTime, setDateTime] = useState(new Date());

  function showTimePicker() {
    setIsTimePickerVisible(true);
  }

  function hideTimePicker() {
    setIsTimePickerVisible(false);
  }

  function showDatePicker() {
    console.log('showDatePicker');
    setIsDatePickerVisible(true);
  }

  function hideDatePicker() {
    setIsDatePickerVisible(false);
  }
  function saveNewDate(newDate) {
    dateTime.setUTCFullYear(newDate.getUTCFullYear());
    dateTime.setUTCMonth(newDate.getUTCMonth());
    dateTime.setUTCDate(newDate.getUTCDate());
    setDateTime(dateTime);
    props.onChangeDateTime(dateTime);
  }

  function displayNewDateFormatted(newDate) {
    const dateFormat = new Intl.DateTimeFormat('fr-FR', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
    }).format(newDate);
    setDate(dateFormat);
  }

  function handleDatePicked(newDate) {
    saveNewDate(newDate);
    displayNewDateFormatted(newDate);
    hideDatePicker();
  }

  function saveNewDateTime(newTime) {
    dateTime.setUTCHours(newTime.getUTCHours());
    dateTime.setUTCMinutes(newTime.getUTCMinutes());
    dateTime.setUTCSeconds(newTime.getUTCSeconds());
    dateTime.setUTCMilliseconds(newTime.getUTCMilliseconds());
    props.onChangeDateTime(dateTime);
  }

  function displayNewDateTimeFormatted(newTime) {
    const timeFormat = new Intl.DateTimeFormat('fr-FR', {
      hour: 'numeric',
      minute: 'numeric',
    }).format(newTime);
    setTime(timeFormat);
  }

  function handleTimePicked(newTime) {
    saveNewDateTime(newTime);
    displayNewDateTimeFormatted(newTime);
    hideTimePicker();
  }
  return (
    <View
      style={{
        paddingTop: 20,
      }}>
      <View style={[styles.titleContainer]}>
        <Text style={styles.titleFont}>Date et heure du rendez-vous</Text>
      </View>
      <View style={styles.dateTimePickerContainer}>
        <TouchableOpacity
          onPress={showDatePicker}
          style={[styles.textInputContaine, styles.paddingRight]}>
          <MDCIcon
            name="calendar-outline"
            size={20}
            color="white"
            style={styles.textIcon}
          />
          <TextInput
            style={styles.textInput}
            autoCapitalize="none"
            placeholder="Date"
            value={date}
            editable={false}
            selectTextOnFocus={false}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={showTimePicker}
          style={styles.textInputContaine}>
          <MDCIcon
            name="clock-outline"
            size={20}
            color="white"
            style={styles.textIcon}
          />
          <TextInput
            style={styles.textInput}
            autoCapitalize="none"
            placeholder="Heure"
            value={time}
            editable={false}
            selectTextOnFocus={false}
          />
        </TouchableOpacity>
        <DateTimePicker
          isVisible={isDatePickerVisible}
          onConfirm={handleDatePicked}
          onCancel={hideDatePicker}
          mode={'date'}
        />
        <DateTimePicker
          isVisible={isTimePickerVisible}
          onConfirm={handleTimePicked}
          onCancel={hideTimePicker}
          mode={'time'}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  dateTimePickerContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
  },
  textIcon: {
    color: 'grey',
    display: 'flex',
    alignSelf: 'center',
  },
  textInputContaine: {
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  textInput: {
    height: 40,
    fontSize: 15,
    marginLeft: 5,
    borderColor: '#9b9b9b',
    borderBottomWidth: 1,
    display: 'flex',
    flexGrow: 1,
  },
  paddingRight: {
    marginRight: 30,
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    alignItems: 'flex-start',
    marginBottom: 15,
  },
  titleFont: {
    fontWeight: 'bold',
  },
});
