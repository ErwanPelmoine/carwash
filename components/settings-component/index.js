import React, {useState} from 'react';
import {Button, Text, View} from 'react-native';
import {baseStyle} from '../../constants/baseComponent';
import auth from '@react-native-firebase/auth';

export default function Settings(props) {
  const [errorMessage, setErrorMessage] = useState(null);

  function handleLogOut() {
    auth()
      .signOut()
      .then(() => {
        props.navigation.navigate('Login');
      })
      .catch(error => setErrorMessage(error.message));
  }
  return (
    <View style={baseStyle.baseContainer}>
      <Text style={baseStyle.title}>Paramètre</Text>
      {errorMessage && <Text style={{color: 'red'}}>{errorMessage}</Text>}
      <Button title="Log out" color="#e93766" onPress={handleLogOut} />
    </View>
  );
}
Settings.navigationOptions = {
  title: 'Paramètres',
};
