import React from 'react';
import {Text, View} from 'react-native';
import {baseStyle} from '../../constants/baseComponent';

export default function PastCommand() {
  return (
    <View style={baseStyle.baseContainer}>
      <Text style={baseStyle.title}>historique</Text>
    </View>
  );
}
PastCommand.navigationOptions = {
  title: 'Historique',
};
