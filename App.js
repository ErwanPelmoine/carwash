import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import MaterialCommunityIconsIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Loading from './components/loading-component/index.js';
import Login from './components/authentication/login-component/login.js';
import SignUp from './components/authentication/sign-up-component/index.js';
import Lavage from './components/lavage-component/lavage.js';
import PastCommand from './components/past-command-component/index';
import Settings from './components/settings-component/index';
import MapInput from './components/lavage-component/map-container-component/map-input-component/index';
import './config/fix';

const AuthStackNavigator = createStackNavigator({
  Login: {
    screen: Login,
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      headerVisible: false,
    },
  },
  /*ForgotPassword: {
    screen: ForgotPassword,
    navigationOptions: {
      headerVisible: false,
    },
  },*/
});

const LavageStackNavigator = createStackNavigator({
  Lavage: {
    screen: Lavage,
    navigationOptions: {
      title: 'Commander un lavage',
    },
  },
  MapInput: {
    screen: MapInput,
    navigationOptions: {
      title: 'Rechercher',
    },
  },
});

const getTabBarIcon = (navigation, focused, tintColor) => {
  const {routeName} = navigation.state;
  if (routeName === 'Lavage') {
    return (
      <MaterialCommunityIconsIcon name={'water'} size={25} color={tintColor} />
    );
  } else if (routeName === 'Settings') {
    return (
      <MaterialCommunityIconsIcon
        name={'settings-outline'}
        size={25}
        color={tintColor}
      />
    );
  } else if (routeName === 'PastCommand') {
    return <MaterialIcon name={'history'} size={25} color={tintColor} />;
  }
};

const tabNavigator = createBottomTabNavigator(
  {
    Lavage: {
      screen: LavageStackNavigator,
      title: 'lavage',
    },
    PastCommand: {
      screen: PastCommand,
    },
    Settings: {
      screen: Settings,
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) =>
        getTabBarIcon(navigation, focused, tintColor),
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  },
);

const App = createSwitchNavigator(
  {
    Loading,
    AuthStackNavigator,
    tabNavigator,
  },
  {
    initialRouteName: 'Loading',
  },
);

export default createAppContainer(App);
